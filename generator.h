#ifndef GENERATOR_H
#define GENERATOR_H
#include "syntax.h"

typedef enum return_type {INT, PINT} ReturnType;

void extract_string_one(Table *table, char *buffer, int len, int hash, int table_capacity);
void extract_string_more(Table *table, char *buffer, int hash, int table_capacity);
int extract_number_one(Table *table, int hash, int table_capacity);
void extract_number_more(Table *table, int *buffer, int hash, int table_capacity);
void app_int(FILE *file, char *name, int *type, int len, int dent);
void app_pint(FILE *file, char *name, int *type, int len, int dent);
// TODO DONE app_text_one just use fprintf
#endif
