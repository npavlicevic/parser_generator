#
# Makefile 
# parser generator
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic -DMORE
OBJ_FLAGS=-c -static
AR_FLAGS=rcs
LIBS=-lm -ltable -lparserconfig -lparsersyntax

FILES=generator.h generator.c generator_main.c
CLEAN=generator_main

all: main 

main: ${FILES}
	${CC} ${CFLAGS} generator.c generator_main.c ${LIBS} -o generator_main

obj: ${FILES}
	${CC} ${OBJ_FLAGS} syntax.c -o syntax.o

lib: ${FILES}
	${AR} ${AR_FLAGS} libparsersyntax.a syntax.o

clean: ${CLEAN}
	rm $^
