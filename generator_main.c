#include "generator.h"

int main(int argc, char **args) {
  int is_correct_literal_;
  int is_correct_tag_;
  int is_correct_comment_;
  int is_correct_number_;
  int is_correct_real_;
  int line_max_depth_;
  int app_name_hash;
  int include_hash;
  int option_count_hash;
  int option_count_extract;
  int argument_count_hash;
  int argument_count_extract;
  int input_length_hash;
  int input_length_extract;
  int buffer_length_hash;
  int buffer_length_extract;
  int return_type_hash;
  int option_hash;
  int i;
  int table_capacity;
  int *buffer_extract_return;
  Line *line;
  Line *line_;
  Table *table;
  IsCorrect *is_correct;
  FILE *file, *app;
  int buffer_capacity;
  char *buffer;
  char *buffer_extract_name;
  char *buffer_extract_include;
  char *buffer_extract_option;
  char app_name[] = ".name";
  char include[] = ".include";
  char option_count[] = ".optionCount";
  char argument_count[] = ".argumentCount";
  char input_length[] = ".inputLength";
  char buffer_length[] = ".bufferLength";
  char return_type[] = ".returnType";
  char option[] = ".option";

  if(argc < 3) {
    return 1;
  }

  is_correct = is_correct_make();
  i = 0;
  line = NULL;
  line_ = NULL;
  table_capacity = 2048;
  table = table_create(table_capacity);
  buffer_capacity = 128;
  buffer = (char*)calloc(buffer_capacity, sizeof(char));
  buffer_extract_name = (char*)calloc(buffer_capacity, sizeof(char));
  buffer_extract_include = (char*)calloc(buffer_capacity, sizeof(char));
  buffer_extract_option = (char*)calloc(buffer_capacity, sizeof(char));
  buffer_extract_return = (int*)calloc(buffer_capacity, sizeof(int));
  app_name_hash = hash_string(app_name, strlen(app_name), -1);
  include_hash = hash_string(include, strlen(include), -1);
  option_count_hash = hash_string(option_count, strlen(option_count), -1);
  argument_count_hash = hash_string(argument_count, strlen(argument_count), -1);
  input_length_hash = hash_string(input_length, strlen(input_length), -1);
  buffer_length_hash = hash_string(buffer_length, strlen(buffer_length), -1);
  return_type_hash = hash_string(return_type, strlen(return_type), -1);
  option_hash = hash_string(option, strlen(option), -1);

  is_correct_compile(is_correct);
  printf("file name %s \n", args[1]);
  file = fopen(args[1], "r");
  app = fopen(args[2], "w");

  for(;1; i++) {
    line = line_read(file, i, buffer, buffer_capacity);

    if(line == NULL) {
      break;
    }

    // DONE TODO remove is_correct_line
    // DONE TODO remove is_correct_indent
    line_trim(line, buffer_capacity);
    line_hash(line);
    is_correct_literal_ = is_correct_literal(is_correct, line);

    is_correct_comment_ = is_correct_comment(is_correct, line);
    if(!is_correct_comment_) {
      line_destroy(&line);
      continue;
    }

    is_correct_tag_ = is_correct_tag(is_correct, line);
    if(!is_correct_tag_) {
      line_cast_tag(line);
    }

    is_correct_number_ = is_correct_number(is_correct, line);
    if(!is_correct_number_) {
      line_ = line_cast_number(line);
    }

    is_correct_real_ = is_correct_real(is_correct, line);
    if(!is_correct_real_) {
      line_ = line_cast_real(line);
    }

    if(!(is_correct_number_ && is_correct_real_)) {
      line_destroy(&line);
      line = line_;
    }

    line_print(line);
    line_print_type(line);
    printf("depth %d ", line->depth);
    printf("is correct literal %d ", is_correct_literal_);
    printf("is correct tag %d ", is_correct_tag_);
    printf("is correct number %d ", is_correct_number_);
    printf("is correct real %d \n", is_correct_real_);
    table_insert(table, line, hash_mirror_int(&(line->line_number)));
  }

  line_max_depth_ = line_max_depth(table);
  printf("\nmaximum depth %d \n\n", line_max_depth_);
  build_symbol_tree(table, line_max_depth_);
  print_symbol_tree(table, i);

  extract_string_one(table, buffer_extract_name, buffer_capacity, app_name_hash, table_capacity);
  printf("\napplication name : %s, hash: %d\n", buffer_extract_name, app_name_hash);
  extract_string_one(table, buffer_extract_include, buffer_capacity, include_hash, table_capacity);
  printf("include : %s, hash: %d\n", buffer_extract_include, include_hash);
  option_count_extract = extract_number_one(table, option_count_hash, table_capacity);
  printf("option count : %d, hash: %d\n", option_count_extract, option_count_hash);
  argument_count_extract = extract_number_one(table, argument_count_hash, table_capacity);
  printf("argument count : %d, hash: %d\n", argument_count_extract, argument_count_hash);
  input_length_extract = extract_number_one(table, input_length_hash, table_capacity);
  printf("input length : %d, hash: %d\n", input_length_extract, input_length_hash);
  buffer_length_extract = extract_number_one(table, buffer_length_hash, table_capacity);
  printf("buffer length : %d, hash: %d\n", buffer_length_extract, buffer_length_hash);
  extract_number_more(table, buffer_extract_return, return_type_hash, table_capacity);
  printf("return type : %d %d %d %d %d, hash: %d\n", buffer_extract_return[0], buffer_extract_return[1],buffer_extract_return[2],buffer_extract_return[3],buffer_extract_return[4], return_type_hash);
  extract_string_more(table, buffer_extract_option, option_hash, table_capacity);
  printf("options : %s\n", buffer_extract_option);

  fprintf(app, "#include \"%s\"\n", buffer_extract_include);
  fprintf(app, "int main(int argc, char **argv) {\n");
  fprintf(app, "%*cint option, r, *R;\n", 2, ' ');
  fprintf(app, "%*cint rLen, inCount;\n", 2, ' ');
  fprintf(app, "%*cint *in, *inFrom, *inTo;\n", 2, ' ');
  fprintf(app, "%*cargv++;\n", 2, ' ');
  fprintf(app, "%*coption = atoi(*argv);\n", 2, ' ');
  fprintf(app, "%*cargv++;\n", 2, ' ');
  fprintf(app, "%*cinCount = atoi(*argv);\n", 2, ' ');
  fprintf(app, "%*cargv++;\n", 2, ' ');
  fprintf(app, "%*crLen = atoi(*argv);\n", 2, ' ');
  fprintf(app, "%*cargv++;\n", 2, ' ');
  fprintf(app, "%*cin = calloc(inCount, sizeof(int));\n", 2, ' ');
  fprintf(app, "%*cinFrom = in;\n", 2, ' ');
  fprintf(app, "%*cinTo = inFrom + inCount;\n", 2, ' ');
  fprintf(app, "%*cfor(; inFrom<inTo;inFrom++,argv++) {\n", 2, ' ');
  fprintf(app, "%*c*inFrom = atoi(*argv);\n", 4, ' ');
  fprintf(app, "%*c}\n", 2, ' ');
  app_int(app, buffer_extract_option, buffer_extract_return, option_count_extract, 2);
  app_pint(app, buffer_extract_option, buffer_extract_return, option_count_extract, 2);
  fprintf(app, "%*cEND: free(in);\n", 2, ' ');
  fprintf(app, "%*creturn 0;\n", 2, ' ');
  fprintf(app, "}\n");

  is_correct_destroy(&is_correct);
  line_table_destroy(table);
  table_destroy(&table);
  fclose(file);
  fclose(app);
  free(buffer);
  free(buffer_extract_name);
  free(buffer_extract_include);
  free(buffer_extract_option);
  free(buffer_extract_return);

  return 0;
}
