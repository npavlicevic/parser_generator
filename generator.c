#include "generator.h"

/*
* Function: extract_string_one
* 
* extract single string
*
* return: void
*/
void extract_string_one(Table *table, char *buffer, int len, int hash, int table_capacity) {
  int i;
  Line *line;

  for(i=0;i<table_capacity;i++) {
    line = (Line*)table->value[i];
    if(line == NULL) {
      continue;
    }
    if(line->parent_hash == hash) {
      memcpy(buffer, line->value.text, len);
      break;
    }
  }
}
/*
* Function: extract_string_more
* 
* extract more than a single string
*
* return: void
*/
void extract_string_more(Table *table, char *buffer, int hash, int table_capacity) {
  int i;
  Line *line;

  for(i=0;i<table_capacity;i++) {
    line = (Line*)table->value[i];
    if(line == NULL) {
      continue;
    }
    if(line->parent_hash == hash) {
      memcpy(buffer, line->value.text, strlen(line->value.text));
      buffer = buffer + strlen(line->value.text);
      *buffer = '\0';
      buffer = buffer + 1;
    }
  }
}
/*
* Function: extract_number_one
* 
* extract single number
*
* return: void
*/
int extract_number_one(Table *table, int hash, int table_capacity) {
  int i;
  Line *line;

  for(i=0;i<table_capacity;i++) {
    line = (Line*)table->value[i];
    if(line == NULL) {
      continue;
    }
    if(line->parent_hash == hash) {
      return line->value.number;
    }
  }

  return -1;
}
/*
* Function: extract_number_more
* 
* extract more than single number
*
* return: void
*/
void extract_number_more(Table *table, int *buffer, int hash, int table_capacity) {
  int i,j = 0;
  Line *line;

  for(i=0;i<table_capacity;i++) {
    line = (Line*)table->value[i];
    if(line == NULL) {
      continue;
    }
    if(line->parent_hash == hash) {
      buffer[j++] = line->value.number;
    }
  }
}
/*
* Function: app_int
* 
* print integer options
*
* return: void
*/
void app_int(FILE *file, char *name, int *type, int len, int dent) {
  int i = 0;

  for(;i<len;i++) {
    if(type[i] == INT) {
      fprintf(file, "%*cif(option == %d) {\n", dent, ' ', i);
      fprintf(file, "%*cr = %s(in);\n", 2*dent, ' ',  name);
      fprintf(file, "%*cprintf(\"%%d \\n\",r);\n", 2*dent, ' ');
      fprintf(file, "%*cgoto END;\n", 2*dent, ' ');
      fprintf(file, "%*c}\n", dent, ' ');
    }
    name = name + strlen(name) + 1;
  }
}
/*
* Function: app_pint
* 
* print integer pointer options
*
* return: void
*/
void app_pint(FILE *file, char *name, int *type, int len, int dent) {
  int i = 0;

  for(;i<len;i++) {
    if(type[i] == PINT) {
      fprintf(file, "%*cif(option == %d) {\n",dent, ' ', i);
      fprintf(file, "%*cR = %s(in);\n", 2*dent, ' ', name);
      fprintf(file, "%*cprintResult(R, rLen);\n", 2*dent, ' ');
      fprintf(file, "%*cfree(R);\n", 2*dent, ' ');
      fprintf(file, "%*cgoto END;\n", 2*dent, ' ');
      fprintf(file, "%*c}\n", dent, ' ');
    }
    name = name + strlen(name) + 1;
  }
}
